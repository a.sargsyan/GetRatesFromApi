﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrensyRate
{
    class Currency:Attribute
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("timestamp")]
        public int Timestamp { get; set; }
        [JsonProperty("base")]
        public string Base { get; set; }
        [JsonProperty("date")]
        public DateTime Date { get; set; }
        [JsonProperty("rates")]
        public Dictionary<string, double> Rates { get; set; }
    }
}
