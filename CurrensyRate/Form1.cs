﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurrensyRate
{
    public partial class Form1 : Form
    {
        Currency[] last3day;
        string Combo1="EUR";
        string Combo2="AMD";
        string AccessKey = "38112033d9ee4a117b1424ef157e34e1";
        public Form1()
        {
            InitializeComponent();
        }
        private Currency GetRatesByDte(DateTime day)
        {
            if (DayOfWeek.Saturday == day.DayOfWeek)
                day = day.AddDays(-1);
            if (DayOfWeek.Sunday == day.DayOfWeek)
                day = day.AddDays(-2);
            string date = day.ToString("yyyy-MM-dd");
            var url = $"http://data.fixer.io/api/{date}?access_key={AccessKey}&symbols=GBP,AMD,USD,GEL,RUB,EUR";
            WebClient w = new WebClient();
            var json = w.DownloadString(url);
            return JsonConvert.DeserializeObject<Currency>(json);
        }
        private Currency[] GetRangeRates(int days)
        {
            DateTime date = DateTime.Now;

            var rate = new Currency[days];
            for (int i = 0; i < days; i++)
            {
                rate[i] = GetRatesByDte(date);
                date = rate[i].Date.AddDays(-1);
            }
            return rate;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Combo1 = comboBox1.Text;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
             last3day = GetRangeRates(3);

            string[] items = last3day[0].Rates.Keys.ToArray();
            
            comboBox1.Items.AddRange(items);
            comboBox2.Items.AddRange(items);

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= 100; i++)
                progressBar1.Value = i;
            last3day = GetRangeRates(3);
            // today
            label10.Text = Convert.ToString(last3day[0].Rates[Combo2]/last3day[0].Rates[Combo1]);
            
            //one dat ago
            label11.Text = Convert.ToString(last3day[1].Rates[Combo2] / last3day[1].Rates[Combo1]); ;
            //two dat ago
            label12.Text = Convert.ToString(last3day[2].Rates[Combo2] / last3day[2].Rates[Combo1]);
            double difference = Convert.ToDouble(label10.Text) - Convert.ToDouble(label12.Text);
            label13.Text =Convert.ToString(difference);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Combo2 = comboBox2.Text;
        }

        private void label13_Click(object sender, EventArgs e)
        {            
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
           
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }        
    }
}
