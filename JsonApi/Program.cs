﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;

namespace JsonApi
{
    class Program
    {
        static void Main(string[] args)
        {
            //access_key=38112033d9ee4a117b1424ef157e34e1
            Api[] last3day = GetRangeRates(3);
            var s=last3day[0].Rates["AMD"]/last3day[0].Rates["USD"];
             
        }
        static Api GetRatesByDte(DateTime day)
        {
            if (DayOfWeek.Saturday==day.DayOfWeek)
                day=day.AddDays(-1);
            if (DayOfWeek.Sunday==day.DayOfWeek)
                day=day.AddDays(-2);
            string date =day.ToString("yyyy-MM-dd");
            var url = $"http://data.fixer.io/api/{date}?access_key=38112033d9ee4a117b1424ef157e34e1&symbols=GBP,AMD,USD,GEL,RUB";
            WebClient w = new WebClient();
            var json = w.DownloadString(url);
            return JsonConvert.DeserializeObject<Api>(json);
        }
        static Api[] GetRangeRates(int days)
        {
            DateTime date = DateTime.Now;

            Api[] rate = new Api[days];
            for (int i = 0; i < days; i++)
            {
                rate[i] = GetRatesByDte(date);
                date = rate[i].Date.AddDays(-1);
            }
            return rate;
        }
    }
}
