﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace test
{
    namespace ConsoleProgram
    {
        public class Class1
        {
            private const string URL = "http://data.fixer.io/api/latest?access_key=38112033d9ee4a117b1424ef157e34e1&base=AMD&symbols=USD";
            private string Data= @"{""object"":{""date"":""Date""}}";
            //[JsonProperty("success")]
            public bool Success { get; set; }
            //[JsonProperty("timestamp")]
            public int Timestamp { get; set; }
            //[JsonProperty("date")]
            public string Date { get; set; }
            //[JsonProperty("base")]
            public string Base = "AMD";
            //[JsonProperty("rates")]
            public Dictionary<string, double> Rates = new Dictionary<string, double>();        

            private static void CreateObject()
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Method = "POST";
                request.ContentType = "application/json";
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    Console.Out.WriteLine(response);
                    responseReader.Close();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("-----------------");
                    Console.Out.WriteLine(e.Message);
                }

            }
            static void Main(string[] args)
            {
                Class1.CreateObject();
            }
        }
    }
}
